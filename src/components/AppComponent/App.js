import React from 'react';
import styles from './App.css';

export default function App() {
	const [ messages, _ ] = React.useState([
		'Custom React App',
		'Modular CSS',
		'Fully Customizable',
		'create-react-app size 151.7 MB',
		'custom-react-app size 66.5 MB',
		'Please refer to the README.md for instructions.'
	]);
	const [ index, setIndex ] = React.useState(0);
	const [ moving, setMoving ] = React.useState(true);

	function tick() {
		if (moving) {
			setIndex((index + 1) % messages.length);
		}
	}
	useInterval(tick, moving ? 2000 : null);

	function handleChangeMoving(e) {
		setMoving(!moving);
	}

	return (
		<React.Fragment>
			<h1
				className={styles.dialog}
				onClick={handleChangeMoving}
			>
				{messages[index]}
			</h1>
		</React.Fragment>
	);
}

function useInterval(callback, delay) {
	const savedCallback = React.useRef();

	React.useEffect(
		() => {
			savedCallback.current = callback;
		},
		[ callback ]
	);

	React.useEffect(
		() => {
			function tick() {
				savedCallback.current();
			}
			if (delay !== null) {
				let tock = setInterval(tick, delay);
				return () => clearInterval(tock);
			}
		},
		[ delay ]
	);
}
